const users = [
  'azka',
  'rozy',
  'furqon',
  'irfan',
  'chris',
  'bagas',
  'ricko',
  'nicko',
  'yudi',
  'cip',
  'irwin'
]

const items = {
  foods: [
    {
      id: 1,
      price: 80000,
      name: 'Summer Caprese Salad',
      ingredients: 'tomatoes, peaches, cucumber, fresh mozzarella cheese, red onion, basil, and a simple balsamic glaze',
      description: 'Summer Caprese Salad made with tomatoes, peaches, cucumber, fresh mozzarella cheese, red onion, basil, and a simple balsamic glaze. This easy salad is a great side dish to any summer meal.',
      favorite: false,
      img: 'https://www.twopeasandtheirpod.com/wp-content/uploads/2017/07/Summer-Caprese-Salad-4.jpg',
      recommended: false,
      hottest: false,
      popular: false,
      newCombo: false,
      top: false
    },
    {
      id: 2,
      price: 80000,
      name: 'Quinoa Fruit Salad',
      ingredients: 'Quinoa with blueberries, strawberries, mango, and a refreshing honey lime dressing',
      description: 'If you are looking for a new fruit salad to make this summer, give this easy and healthy Quinoa Fruit Salad with Honey Lime Dressing a try! It goes well with any summer meal and is a great way to enjoy all of the fine fruits summer has to offer.',
      favorite: false,
      img: 'https://www.twopeasandtheirpod.com/wp-content/uploads/2012/06/Quinoa-Fruit-Salad1.jpg',
      recommended: false,
      hottest: true,
      popular: false,
      newCombo: false,
      top: false
    },
    {
      id: 3,
      price: 80000,
      name: 'Cobb Salad',
      ingredients: 'chicken, bacon, tomatoes, hard boiled eggs, Avocado, blue cheese, chives, and a simple red wine vinegar dressing',
      description: 'This colorful salad is a beauty! The presentation really makes the salad and it doesn’t take long. You can’t go wrong with a classic Cobb Salad!',
      favorite: true,
      img: 'https://www.twopeasandtheirpod.com/wp-content/uploads/2018/07/COBB-SALAD-3.jpg',
      recommended: false,
      hottest: true,
      popular: true,
      newCombo: false,
      top: false
    },
    {
      id: 4,
      price: 80000,
      name: 'Fall Chickpea Salad',
      ingredients: 'Chickpeas, Apple, Avocado, lemon juice, red onion, pecans, dried cranberries, and feta cheese',
      description: 'You will love the textures and flavors of this simple and healthy chickpea salad. It’s made with chickpeas, apple, avocado, dried cranberries, pecans, red onion, feta cheese, and tossed in a light apple cider dressing. It’s perfect for an easy fall lunch or dinner.',
      favorite: false,
      img: 'https://www.twopeasandtheirpod.com/wp-content/uploads/2020/11/Fall-Chickpea-Salad-4.jpg',
      recommended: true,
      hottest: false,
      popular: true,
      newCombo: false,
      top: false
    },
    {
      id: 5,
      price: 80000,
      name: 'Roasted Delicata Squash Salad',
      ingredients: 'greens, maple roasted delicata squash, dried cranberries, avocado, feta cheese, candied pecans, and a simple balsamic dressing',
      description: 'This beautiful Roasted Delicata Squash Salad is the perfect fall salad. It’s a great salad for Thanksgiving or any fall meal.',
      favorite: false,
      img: 'https://www.twopeasandtheirpod.com/wp-content/uploads/2020/11/delicata-squash-4.jpg',
      recommended: false,
      hottest: true,
      popular: false,
      newCombo: true,
      top: false
    },
    {
      id: 6,
      price: 80000,
      name: 'Favorite Fruit Salad',
      ingredients: 'Pineapple, Blackberries, Strawberries, Blueberries, and Kiwi',
      description: 'This Fruit Salad with Citrus Poppy Seed Dressing is colorful, sweet, and the perfect side dish to any meal. It’s refreshing, healthy, and always a crowd pleaser. Feel free to mix up the fruit, depending on what is in season.',
      favorite: false,
      img: 'https://www.twopeasandtheirpod.com/wp-content/uploads/2020/07/fruit-salad-2.jpg',
      recommended: false,
      hottest: false,
      popular: false,
      newCombo: true,
      top: false
    },
    {
      id: 7,
      price: 80000,
      name: 'Steak Salad',
      ingredients: 'Steak, Salad greens, Corn, Tomatoes, Avocado, Red onion, Pepitas, Queso fresco, and Dressing',
      description: 'Grilled Steak Salad with sweet corn, tomatoes, avocado, red onion, and a creamy cilantro lime dressing. You will love this fresh and simple summer salad!',
      favorite: false,
      img: 'https://www.twopeasandtheirpod.com/wp-content/uploads/2020/08/steak-salad-1-1.jpg',
      recommended: true,
      hottest: false,
      popular: true,
      newCombo: true,
      top: false
    },
    {
      id: 8,
      price: 80000,
      name: 'Green Bean Salad',
      ingredients: 'tomatoes, Parmesan, fresh basil, and a little bit of red onion',
      description: 'This is the best Green Bean Salad Recipe! It’s filled with tomatoes, Parmesan, fresh basil, and a little bit of red onion. I top it off with a zesty homemade dressing and it’s the most perfect side dish for summertime.',
      favorite: false,
      img: 'https://www.twopeasandtheirpod.com/wp-content/uploads/2020/07/Green-Bean-Salad-4.jpg',
      recommended: false,
      hottest: false,
      popular: false,
      newCombo: false,
      top: true
    }
  ]
}

module.exports = {
  users,
  items
}