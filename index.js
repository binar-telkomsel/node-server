const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

const app = express();
const port = 5001;
const { users, items } = require('./data')

app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Hello World!')
});

app.get('/ping', (req, res) => {
  res.status(200).send({
    status: 'success',
    message: 'success',
    data: 'pong!'
  })
})

app.post('/login', (req, res) => {
  const { username } = req.body;

  const user = users.filter((item) => item === username)

  if (!username || user.length === 0) {
    res.status(400).send({
      status: 'error',
      message: 'error'
    })
  } else {
    const token = jwt.sign({
      data: req.body
    }, 'secrett', { expiresIn: 60 * 60 });

    res.status(200).send({
      status: 'success',
      message: 'success',
      data: {
        ...req.body,
        token
      }
    }) 
  }
})

app.get('/home', (req, res) => {
  const { username } = req.query;
  const { authorization } = req.headers;
  const { foods } = items;


  const token = authorization;
  const user = users.filter((item) => item === username)

  if (!token) {
    res.status(401).send({
      status: 'error',
      message: 'Unauthorized'
    })
  }
  try {
    const decoded = jwt.verify(token, 'secrett');

    if (!username || user.length === 0) {
      res.status(400).send({
        status: 'error',
        message: 'error'
      })
    } else {
      res.status(200).send({
        status: 'success',
        message: 'success',
        data: {
          foods
        }
      })
    }
  } catch(err) {
    res.status(401).send({
      status: 'error',
      message: 'Unauthorized'
    })
  }
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});